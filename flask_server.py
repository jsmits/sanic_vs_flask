from flask import Flask
from flask import jsonify
import redis

app = Flask(__name__)
app.redis_pool = redis.ConnectionPool(
    host='localhost', port=6379, db=0, max_connections=10)


def counter():
    rc = redis.Redis(connection_pool=app.redis_pool)
    count = rc.execute_command('incr', 'count')
    log_1 = rc.execute_command('set', 'log-1-'.format(count), 1)
    log_2 = rc.execute_command('set', 'log-2-'.format(count), 2)
    return count


@app.route("/")
def test():
    count = counter()
    return jsonify({"hello": "world", "count": count})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8001)

