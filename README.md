sanic vs flask
==============

To test performance differences between async vs non-async web servers, I created two simple web servers, a [sanic](https://sanic.readthedocs.io) server and a flask server. With each request, both servers increment a redis variable and add two log lines to redis. Performance is measured with the `ab` tool. Both servers use redis connection pooling.


To get started, create a virtualenv with python >= 3.5 (tested with 3.6.1) and install the dependencies:

    $ pip install -r requirements.txt


redis
-----

Make sure you have a local redis server running, serving requests on localhost:6379, e.g. with docker:

    $ docker run -it --rm -p 6379:6379 redis


async server
------------

Test (async) `sanic_server.py` performance by::

    $ python sanic_server.py

And then in a different terminal::

    $ ab -n 10000 -c 50 http://localhost:8000/

If you don't have `ab` you can install it with `apt-get`:

    $ sudo apt-get install apache2-utils


non-async server
----------------

Test (non-async) `flask_server.py` performance by:

    $ python flask_server.py

And then in a different terminal::

    $ ab -n 10000 -c 50 http://localhost:8001/


On my machine there were about 2.4 times more requests handled by the sanic
server (2850 per/sec.) than by the flask server (1185 per/sec.). Also, the sanic server uses 100% CPU, which makes sense, since it uses an event loop, while the flask server only gets to about 86% CPU usage on my machine.

Some thoughts:

- async code is a little  more complex. You need more boilerplate to get going. And it'll most likely be harder to write tests for.

- async code can handle considerably more requests per second than sync code (about 2.4 times more).

- async server is more efficient (I mean, it can use 100% CPU)
