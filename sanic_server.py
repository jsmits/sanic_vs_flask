import aioredis
from sanic import Sanic
from sanic.response import json

app = Sanic()


async def count_and_log(conn):
    count = await conn.execute('incr', 'count')
    # log some messages to try to make it slower
    log_1 = await conn.execute('set', 'log-1-{}'.format(count), 1)
    log_2 = await conn.execute('set', 'log-2-{}'.format(count), 2)
    return count


async def counter():
    async with app.redis_pool.get() as rc:
        # beter to use the low-level connection, because it is faster
        conn = rc.connection
        count = await count_and_log(conn)
        return count


@app.route("/")
async def test(request):
    count = await counter()
    return json({"hello": "world", "count": count})


@app.listener('before_server_start')
async def before_server_start(app, loop):
    app.redis_pool = await aioredis.create_pool(
        ('localhost', 6379),
        minsize=5,
        maxsize=10,
        loop=loop
    )


@app.listener('after_server_stop')
async def after_server_stop(app, loop):
    app.redis_pool.close()
    await app.redis_pool.wait_closed()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)

